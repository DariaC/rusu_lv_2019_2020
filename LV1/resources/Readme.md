Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
Promjene koje sam napravila su: 
-izostavila raw_input i pretvorila u običan string
-u open file dodala r za čitanje i fnamex pretvorila u fname
-u oba printa dodala ()
-umjersto = u counts nakon else: stavila += kako bi brojao
 ukoliko ima više riječi
 
 U ovoj vježbi upoznale su se osnove Gita i Pythona. Koristio se udaljeni
 repozitorij na Gitlab-u i rješavanjem zadanih zadataka koristile su se osnovne Git naredbe 
 poput push, commit, add, branch, merge, checkout, itd... Trebalo je riješiti 
 6 zadataka u Pythonu potpuno samostalno dok se u početku trebalo ispravaljati
 već postojeća datoteka. Rješavajući zadatke upoznalo se sa osnovnim 
 petljama poput for, while, rad sa listom i riječnikom,
 rad s datotekama, input, output i mnoge druge.