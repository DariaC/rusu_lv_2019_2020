# Zadatak 5

# Napišite program koji od korisnika zahtijeva unos imena tekstualne datoteke. 
#Program nakon toga treba tražiti linije
# oblika:
# X-DSPAM-Confidence: <neki_broj>
# koje predstavljaju pouzdanost korištenog spam filtra. 
#Potrebno je izračunati srednju vrijednost pouzdanosti. Koristite
# datoteke mbox.txt i mbox-short.txt
# Primjer
# Ime datoteke: mbox.txt
# Average XC 0.894128046745
# Ime datoteke: mbox-short.txt
# Average X-DSPAM-Confidence: 0.750718518519 

fname=input("Unesite ime datoteke: ")
fname_c = open(fname, "r")
rez=[]
m_val=0
zbr=0
for line in fname_c:
    if line.startswith("X-DSPAM-Confidence:"):
        words=line.split()
        for word in words:
            if word != "X-DSPAM-Confidence:":
                rez.append(word)
                zbr=zbr+float(word)
                m_val=m_val+1
print("Srednja vrijednost je ",zbr/m_val)
   
            
            
