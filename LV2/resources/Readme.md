### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

U ovoj vježbi radilo se sa regularnim izrazima i grafičkim prikazom. Prva dva zadatka su se odnosila na regex. 
Trebalo je raditi sa fileovima i pretvoriti ih u string kako bi se mogli pretraživati na odgovarajući način. O rješenju je
uvelike ovisio oblik stringa u koji smo spremali file. Pronađeni podatci su se spremali u listu.
U daljnjim zadatcima radilo se sa histogramom i običnom plot funkcijom. Trebalo je raditi i sa csv datotekom što 
je zahtjevalo uključivanje i drugih biblioteka za rad sa csv dat (bilo je više opcija i nisam uzela pandas pošto se to radi u idućoj
vježbi i da se proširi znanje). Kao zadnji zadatak bio je posvijetliti sliku što se jednostavno moglo množenjem svih polja sa
nekim brojem (nenegativnim). 