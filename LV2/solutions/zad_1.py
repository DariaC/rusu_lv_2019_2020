#Zadatak 1
# Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći 
# valjane email adrese te izdvojiti samo prvi dio
# adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz. 
# Koristite datoteku mbox-short.txt. Ispišite rezultat. 
import re
def listToString(s):  
    str1 = ""   
    for ele in s:  
        str1 = str1 + " " + ele
    return str1 
emails=[]
file=open("mbox-short.txt", "r")
for line in file:
    words = line.split()
    for word in words:
        if "@" in word:
            emails.append(word)
string= listToString(emails)
# String koji pregledava reg izraz u nastavku sastoji se od emailova
# koji su odvojeni jednim razmakom 
Reg_emails=re.findall("\s+<*(\S*)@", string)
print(Reg_emails)


