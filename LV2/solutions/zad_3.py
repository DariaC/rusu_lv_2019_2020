# Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne 
#  vrijednosti 0 ili 1. Neka 1
# označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji 
# sadrži visine osoba koje se dobiju uzorkovanjem
# odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova 
# distribucija sa srednjom vrijednošću 180 cm i
# standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova 
# distribucija sa srednjom vrijednošću 167 cm
# i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) 
# odnosno crvenom bojom (0). Napišite
# funkciju koja računa srednju vrijednost visine za muškarce odnosno žene 
# (probajte izbjeći for petlju pomoću funkcije
# np.dot). Prikažite i dobivene srednje vrijednosti na grafu. 
import numpy as np
import matplotlib.pyplot as plot
np.random.seed(10)
ran_num = np.random.randint(2, size=100) # 2 zbog 0 i 1, drugo je broj dobi
h_fem=[]
h_male=[]
for rnum in ran_num:
    if rnum == 1:
        h_male.append(np.random.normal(187,7))
    else:
        h_fem.append(np.random.normal(167, 7))

plot.hist([h_male, h_fem], color=['blue', 'red'])
avg_m=np.average(h_male)
avg_f=np.average(h_fem)
plot.axvline(avg_m)
plot.axvline(avg_f)
plot.legend(["m", "f"])
