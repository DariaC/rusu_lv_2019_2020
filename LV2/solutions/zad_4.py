# Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). 
# Pomoću histograma prikažite rezultat ovih bacanja. 
import matplotlib.pyplot as plot
import numpy as np
np.random.seed(10)
throw=np.random.randint(1,high=7,size=100)

plot.xlabel('Broj na kocki')
plot.ylabel('Broj bacanja')
plot.title('Histogram bacanja igraće kocke')
plot.xlim(0.5,6.5)
plot.grid(True)
plot.yticks(np.arange(0, 25, 1.0))
plot.hist(throw,bins=15, color='pink')