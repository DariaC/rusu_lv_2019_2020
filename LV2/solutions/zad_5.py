# U direktoriju rusu_lv_2019_20/LV2/resources nalazi se datoteka mtcars.csv 
# koja sadrži različita
# mjerenja provedena na 32 automobila (modeli 1973-74). Prikažite ovisnost 
# potrošnje automobila (mpg) o konjskim
# snagama (hp). Na istom grafu prikažite i informaciju o težini pojedinog 
# vozila. Ispišite minimalne, maksimalne i
# srednje vrijednosti potrošnje automobila. 
import csv
import matplotlib.pyplot as plot

autici_hp=dict()
sorted_autici=dict()
autici_mpg=dict()
autici_wt=dict()
mpg=[]
mpg_sum=0
wt=[]
file=open("mtcars.csv", 'r')
csv_file=csv.DictReader(file)
for line in csv_file:
    autici_hp[line['car']]=int(line['hp']) #stvara riječnik sa autom kao ključem i hp kao pridružene vrijednosti
    autici_mpg[line['car']]=float(line['mpg']) #isto samo sa mpg
    autici_wt[line['car']]=float(line['wt']) #isto samo sa wt 
    mpg_sum+=float(line['mpg'])
sorted_values = sorted(autici_hp.values()) #sortira vrijednosti koje će biti na x osi
for i in sorted_values:
    for k in autici_hp.keys():
        if autici_hp[k]==i:
            sorted_autici[k]=autici_hp[k]  #stvara sortirani riječnik 

for x in sorted_autici:
    if x in autici_mpg.keys():
        mpg.append(autici_mpg[x]) #sprema u listu vrijednosti po redu kakav je u sortiranom riječniku
    if x in autici_wt.keys():
        wt.append(autici_wt[x]) #isto samo za wt ovaj put
#sada imamo poredane vrijednosti hp, mpg i wt i sad se mogu koristiti za graf 
plot.plot(sorted_values, mpg)
for i, txt in enumerate(wt):
    plot.annotate(wt[i], (sorted_values[i], mpg[i]), size=7)
plot.xlabel('Konjska snaga')
plot.ylabel('Potrošnja')
plot.scatter(sorted_values, mpg, color='red')

max_mpg=mpg[0] 
min_mpg=mpg[0]
avg_mpg= mpg_sum/len(mpg)

for item in mpg:
    if item > max_mpg:
        max_mpg = item
    elif item < min_mpg:
        min_mpg = item

print( min_mpg, max_mpg, avg_mpg )


# for hp_el in hp:
#     for line in csv_file: 
#         if int(line['hp']) == hp_el:
#             autici.apped(line['car'])
#             break;

         