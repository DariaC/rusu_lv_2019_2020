 LV-3_RUSU
U ovoj vježbi smo odradili rad sa pandas bibliotekom, uspoznali se s osnovama series i dataframe 
tipom podatka iz pandas. Vježba je imala 3 zadatka: 
U prvom zadatku bio je rad na md datoteci mtcars iz prošle vježbe. Koristile su se osnovne naredbe kako bi se
upoznalo sa rukovanjem pandas i novim tipom podatka. Koristila sam tail, sorted_values, head, describe, mean,
count,..
U drugom zadatku trebalo je također raditi sa mtcars, ali ovaj put malo naprednije radnje  poput kreiranja
različitih vrsta grafova pomoću podataka iz md  datoteke. 
U trećem zadatku trebalo se koristiti podatcima s interneta i odraditi analizu pomoću nekoliko različitih
grafova.
Ovaj put sam napravila vlastitu readme datoteku za razliku od prošlih izvještaja gdje sam nadopunjavala postojeću. 