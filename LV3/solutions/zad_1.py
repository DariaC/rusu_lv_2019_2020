# Za mtcars skup podataka (nalazi se rusu_lv_2019_20/LV3/resources) napišite programski kod koji će
# odgovoriti na sljedeća pitanja:
# 1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
# 2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
# 3. Kolika je srednja potrošnja automobila sa 6 cilindara?
# 4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
# 5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
# 6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
# 7. Kolika je masa svakog automobila u kilogramima?

import pandas as pd


mtcars = pd.read_csv('mtcars.csv') 
#1.
sorted_mtcars=mtcars.sort_values(by=['mpg']) #default asc tako da se čita zadnjih 5 za rez
print(sorted_mtcars.tail(5)) #print auta sa svim podatcima
print(sorted_mtcars[['car', 'mpg']].tail(5)) #samo zdanja 5 auta sa mpg vrijednostima
#2.
cyl8=mtcars[mtcars.cyl==8]
sorted_cyl8=cyl8.sort_values(by=['mpg'])
print(sorted_cyl8.head(3))
#3.
cyl6=mtcars[mtcars.cyl==6]
print(cyl6.describe()) # pročitamo iz info pod mean mpg
print(cyl6['mpg'].mean()) # ili ova opcija
#4.
zad4mtcars=mtcars[(mtcars.cyl==4) & (mtcars.wt > 2.000) & (mtcars.wt < 2.200)]
print(zad4mtcars['mpg'].mean()) #iako postoji samo 1 auto pa može i preko print(zad4mtcars['mpg'])
#5.
autom=mtcars.groupby(['am']).car.count() 
print(autom) # vraća 0 ako nije automatski mjenjač, a 1 ako je 
#6.
print(mtcars[(mtcars.am==1) & (mtcars.hp > 100)].car.count())
#7.
mtcars['wt/kg']=(mtcars.wt * 0.45359)
print(mtcars[['wt/kg', 'wt', 'car']]) 
