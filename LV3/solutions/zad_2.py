# Napišite programski kod koji će iscrtati sljedeće slike za mtcars skup podataka:
# 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
# 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
# 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
# potrošnju od automobila s automatskim mjenjačem?
# 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
# mjenjačem. 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mtcars = pd.read_csv('mtcars.csv') 
wt4=[] # za 2.zadatak da se ne ponavlja for petlja nepotrebno
wt6=[] # za 2.zad
wt8=[] # za 2.zad
am0=[] # za 3.zad
am1=[] # za 3.zad
qsec0=[] # za 4.zad
qsec1=[] # za 4.zad
hp0=[] # za 4.zad
hp1=[] # za 4.zad
#1.
width=0.25 # bez promjene u x cord su svi naslagani jedni na druge (malo nepregledno)
mtcars4=[]
mtcars6=[]
mtcars8=[]
xcord=np.arange(1, len(mtcars)+1, 1) # postavjeno tako da ne počinje od 0 nego od 1
for i in range(len(mtcars)):
    if mtcars.iloc[i]['cyl']==4:
        mtcars4.append(mtcars.iloc[i]['mpg'])
        wt4.append(mtcars.iloc[i]['wt']) # za 2.zadatak da se ne ponavlja for petlja
    else:
        mtcars4.append(0)
    if mtcars.iloc[i]['cyl']==6:
        mtcars6.append(mtcars.iloc[i]['mpg'])
        wt6.append(mtcars.iloc[i]['wt']) # za 2.zad
    else:
        mtcars6.append(0)
    if mtcars.iloc[i]['cyl']==8:
        mtcars8.append(mtcars.iloc[i]['mpg'])
        wt8.append(mtcars.iloc[i]['wt']) # za 2.zad
    else:
        mtcars8.append(0)
    if mtcars.iloc[i]['am']==0: # za 3. i 4. zad
        am0.append(mtcars.iloc[i]['mpg']) # za 3.zad
        qsec0.append(mtcars.iloc[i]['qsec']) #  za 4.zad
        hp0.append(mtcars.iloc[i]['hp']) # za 4.zad
    else:
        am1.append(mtcars.iloc[i]['mpg']) # za 3.zad
        qsec1.append(mtcars.iloc[i]['qsec']) # za 4.zad
        hp1.append(mtcars.iloc[i]['hp']) # za 4.zad
        
plt.figure() # da bi se stvorio plot za svaki od 4 zad
plt.bar(xcord, mtcars4, width, color='teal')
plt.bar(xcord+width, mtcars6, width, color='mediumslateblue')
plt.bar(xcord+2*width, mtcars8, width, color='palevioletred')
plt.xticks(xcord) # da se vidi broj auta na koji se odnose rezulati
plt.legend(['4 cyl', '6 cyl', '8 cyl']) 
plt.title('Potrošnja automobila sa 4, 6 i 8 cilindara, za svaki auto')
plt.xlabel('auto')
plt.ylabel('mpg/potrošnja')

#2.
plt.figure()
plt.boxplot([wt4, wt6, wt8], positions =[4, 6, 8])
plt.title('Težina automobila sa 4, 6 i 8 cilindara')
plt.xlabel('cyl/broj cilindara')
plt.ylabel('wt/težina lbs')
plt.grid(axis='y', linestyle='--') # za iscrtavanje linija za y vrijednosti (wt)
ycord=np.arange(1.25,5.75, 0.25) 
plt.yticks(ycord) # za prikazivanje više vrijednosti (wt) na y osi

#3.
plt.figure()
plt.boxplot([am0, am1], positions=[0,1])
plt.grid(axis='y', linestyle='--')
plt.title('Potršsnja automobila s ručnim i automatskim mjenjačem')
plt.xlabel('am/0=ručni mj. 1=automatski mj.')
plt.ylabel('mpg/potrošnja')

#4.
plt.figure()
plt.scatter(qsec0, hp0, color='fuchsia', marker='*')
plt.scatter(qsec1, hp1, color='indigo', marker='+')
plt.grid(linestyle='--') 
plt.title('Odnos ubrzanja i snage automobila s ručnim i automatskim mjenjačem')
plt.xlabel('qsec/ubrzanje')
plt.ylabel('hp/snaga')
plt.legend(['ručni mj.', 'automatski mj.'])

        

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    