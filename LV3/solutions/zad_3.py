# Na stranici http://iszz.azo.hr/iskzl/exc.htm moguće je dohvatiti podatke o kvaliteti zraka za Republiku Hrvatsku. Podaci
# se mogu preuzeti korištenjem RESTfull servisa u XML ili JSON obliku. U rusu_lv_2019_20/LV3/resources/
# skriptu koja dohvaća podatke te ih pohranjuje u odgovarajući DataFrame. Prepravite/nadopunite skriptu s
# programskim kodom kako bi dobili sljedeće rezultate:
# 1. Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek.
# 2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
# 3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
# 4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
# 5. Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda. 

import urllib.request # dodala .request zbog errora AttributeError: module 'urllib' has no attribute 'urlopen'
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

# url that contains valid xml file: 1.zad
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'
airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))
df2=pd.DataFrame(columns=('mjerenje', 'vrijeme'))
i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1
    
df['vrijeme'] = pd.to_datetime(df['vrijeme'], utc=True) # dodavanje utc=True je riješilo problem pretvorbe
df.plot(y='mjerenje', x='vrijeme')
plt.grid(axis='y', linestyle=':')

df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

#2.
h_value = df.sort_values(by=['mjerenje'])
print(h_value['vrijeme'].tail(3))

#3.
index=np.arange(1,13,1)
missing=[]
for i in range(1,13):   #za svaki mjesec provjeravamo
    month_i= df[df.month==i]
    if (i==1) or (i==3) or(i==5) or(i==7) or (i==8) or (i==10) or (i==12):
        missing.append(31-len(month_i))  #od mjeseci koji imaju 31 dan oduzima dane koji su uneseni za taj mjesec
    elif (i==4) or (i==6) or(i==9) or(i==11):
        missing.append(30-len(month_i))  #od mjeseci koji imaju 30 dana oduzima dane koji su unwseni za taj mjesec
    else:
        missing.append(28-len(month_i)) #od veljace oduzima dane koji su uneseni za taj mjesec
plt.figure()
plt.bar(index, missing)   #index- od 1. do 12.mjeseca
plt.title("Broj izostalih vrijednosti tijekom mjeseci")
plt.xlabel("mjesec")
plt.xticks([1,2,3,4,5,6,7,8,9,10,11,12])
plt.ylabel("broj dana bez unosa mjerenja")
plt.grid(axis='y', linestyle=":")

#4.
eneroC=[] # siječanj (šp) koncentracija
agostoC=[] # kolovoz (šp) koncentracija
enero=df[df.month==1]  # vrijednosti za siječanj
agosto=df[df.month==8] # vrijednosti za kolovoz

for i in enero["mjerenje"]:  # za vrijednosti mjerenja među radnim danima dodaj u listu
    eneroC.append(i)    
    
for i in agosto["mjerenje"]:   
    agostoC.append(i)    
    
plt.figure()
plt.boxplot([eneroC, agostoC]) 
plt.xticks([1, 2], ['Siječanj', 'Kolovoz'])  # 
plt.title("Koncentracija PM10 u sijecnju i kolovozu")
plt.xlabel("mjesec")
plt.ylabel("koncentracija/ⲙ*g/m3")
plt.grid(axis= 'y', linestyle="--") # nema potrebe za grid na x osi
plt.yticks([10,20,30,40,50,60,70,80,90,100,110,120]) # dodavanje više vrijednosti na y-os

#5.
workingW=[]
weekend=[]
days_working=df[(df.dayOfweek==0)|(df.dayOfweek==1)|(df.dayOfweek==2)|(df.dayOfweek==3)|(df.dayOfweek==4)] # vrijednosti iz dataframe za radne dane (0-4 tj pon-pet)
days_weekend=df[(df.dayOfweek==5)|(df.dayOfweek==6)] # vrijednosti iz dataframe za dane vikenda (5-6 tj sub-ned)
        
for i in days_working["mjerenje"]:
    workingW.append(i)
for i in days_weekend["mjerenje"]:
    weekend.append(i)
    
plt.figure()
plt.hist([workingW, weekend], color=['navy','orchid']) 
plt.title("Koncentracija tokom tjedna")
plt.xlabel("koncentracija/ⲙ*g/m3")
plt.ylabel("broj koncentracija u razdoblju")
plt.grid(axis='y', linestyle=':')
plt.legend(["radni tjedan","vikend"])
