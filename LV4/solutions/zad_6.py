# Za primjer 4.2. iz priloga koristite veći broj dodatnih veličina u modelu 
# (npr., degree=15). Međutim, umjesto obične
# linearne regresije koristite Ridge regresiju. Mijenjate vrijednost 
# regularizacijskog parametar. Što primjećujete? Kako
# glase koeficijenti ovog modela, a kako modela iz zadatka 7 za degree = 15. 
# Komentirajte dobivene rezultate. 

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
 
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis] 

poly = PolynomialFeatures(15)
xnew = poly.fit_transform(x) 

np.random.seed(12)
indeksi = np.random.permutation(len(xnew))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)] 

xtrain = xnew[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = xnew[indeksi_test]
ytest = y_measured[indeksi_test]

lambdas = [0.001, 1, 1000] # uzete veće razlike kako bi se mogao bolje vidjeti utjecaj, mjenjanje vrijednosti regulacijskog parametra

plt.figure()

for l in lambdas:
    
    linearModel = lm.Ridge(l) # umjesto lm.LinearRegression(), predstavlja ridge regresiju
    linearModel.fit(xtrain, ytrain)
    plt.plot(x, linearModel.predict(xnew), label=('Lambda = ' + str(l)))

plt.plot(x,y_true,label='f')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1], ytrain, 'ok', label='train')
plt.legend(loc = 4) 
print ('Koeficijenti modela dobiveni korištenjem Ridge regresije: ',linearModel.intercept_,',',linearModel.coef_)
# The attributes of model are .intercept_, which represents the coefficient, 𝑏₀ and .coef_, which represents 𝑏₁

# pretpostavljam da se pitanje odnosi na koef iz prošlog zadatka (5.), a ne  sljedećeg (7.)
linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)
print ('Koeficijenti modela iz 5. zadatka za degree = 15: ',linearModel.intercept_,',',linearModel.coef_)
