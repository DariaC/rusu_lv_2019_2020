# Izvještaj LV5

## Opis vježbe
 Kroz vježbu se upoznaje sa osnovama algoritama za grupiranje podataka korištenjem kmeans algoritma i hijerarhalnog grupiranja.
 
## Zadatak 1.
 Koristi se kmeans na podatcima dobivenim danom funkcijom za generiranje umjetnih podataka. Idealno je zadati broj centara ovisno o broju 
 clustera koji imaju podatci. Biranjem različitog parametra stvaraju se različiti oblici clustera. Najbolje funkcionira na ovalnim clusterima.

## Zadatak 2.
 Pomoču kmeans se može dobiti i kriterijska funkcija. Prolazi se kroz broj clustera od 1 do 20 i crta se funkcija. Idealno je izabrati 
 broj clustera koji se nalazi u pregibu funkcije.

## Zadatak 3.
 Koristi se hijerarhalno grupiranje za podatke korištene u zadatku 1. pomoću linkage funkcije. Upotrebljavala različite metode 
 (single, complete, average, weighted, centroid, median, ward) nađene u opisu funkcije. Rezultati se prikazuju pomoću dendrograma.
 Zapažanjem zaključeno da je najbolja metoda ward, a najgora single.

## Zadatak 4.
 Primjena kmeans za kvantizaciju boje na slici vrši se tako da se odabirom broja clustera bira koliko tonova slika ima. Time broj clustera 
 određuje koliko je kvalitetna slika. Više clustera znači bolja kvaliteta. Ukoliko se koristi 10 clustera slika koristi 10 tonova.
 Kako raste broj clustera smanjuje se jačina razlike između slika zbog broja tonova koje može prepoznati ljudsko oko.

## Zadatak 5.
 Zadatak daje slične rezultate kao prošli, samo za drukčiju sliku.

## Zadatak 6.
 Pisanjem vlastite kmeans funkcije bolje se shvaća kako ona funkcionira. Daje slične, ako ne i iste rezultate kao orginalna funkcija.
 Koristi se na istoj skupini podataka kao i za neke od prijašnjih zadataka. 