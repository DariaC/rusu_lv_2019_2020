# U prilogu vježbe nalazi se funkcija 5.1. koja služi za generiranje umjetnih podataka kako 
# bi se demonstriralo grupiranje podataka. Funkcija prima cijeli broj koji definira željeni 
# broju uzoraka u skupu i cijeli broj (od 1 do 5) koji definira na koji način će se generirati 
# podaci, a vraća generirani skup podataka u obliku numpy polja pri čemu su prvi i drugi stupac
# vrijednosti prve odnosno druge ulazne veličine za svaki podatak.
# Generirajte 500 podataka i prikažite ih na slici. Pomoću scikit-learn ugrađene metode za 
# kmeans odredite centre klastera te svaki podatak obojite ovisno o njegovoj pripadnosti 
# pojedinom klasteru (grupi). Nekoliko puta pokrenite napisani kod. Što primjećujete? 
# Što se događa ako mijenjate način kako se generiraju podaci?

#kod iz pr1
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as clstr

def generate_data(n_samples, flagc):
 if flagc == 1:
    random_state = 365
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
 elif flagc == 2:
    random_state = 148
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) #dodano datasets, X izmjenjen u x radi jednostavnosti
    transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
    x = np.dot(x, transformation)
 elif flagc == 3:
    random_state = 148
    x, y = datasets.make_blobs(n_samples=n_samples, centers = 4, cluster_std=[1.0, 2.5, 0.5, 3.0],random_state=random_state) #dodano centers = 4 (nije radilo bez)
 elif flagc == 4:
    x, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
 elif flagc == 5:
    x, y = datasets.make_moons(n_samples=n_samples, noise=.05)
 else:
    x = []

 return x

n_samples = np.arange(1, 6, 1) # brojevi od 1 do 5 za prolazak kroz sve if opcije u generate_data

'kod za provjeru koliko clustera ima da bi se odredio broj centara'
# for n in n_samples:
#     np.random.seed(10)
#     data=generate_data(500, n)
#     plt.figure(n)
#     plt.scatter(data[:, 0], data[:, 1])

num = [3, 3, 4, 2, 2]  # br centara tj parametar clstr.KMeans

for n in n_samples:
    np.random.seed(10)
    data=generate_data(500, n)
    kmeans = clstr.KMeans(num[n-1]).fit(data)
    plt.figure(n)
    plt.scatter(data[:, 0], data[:, 1], c = kmeans.labels_)
    plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], c = 'fuchsia', marker = 'x') # boja i oznaka postavljeni jer se default ne vidi
    
