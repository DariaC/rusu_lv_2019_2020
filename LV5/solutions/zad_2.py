# Scikit-learn kmeans metoda vraća i vrijednost kriterijske funkcije (5-3). 
# Za broj klastera od 1 do 20 odredite vrijednost kriterijske funkcije za podatke iz Zadatka 1. 
# Prikažite dobivene vrijednosti pri čemu je na x-osi broj klastera (npr. od 2
# do 20), a na y-osi vrijednost kriterijske funkcije. Kako komentirate dobivene rezultate? 
# Kako biste pomoću dobivenog grafa odredili optimalni broj klastera?

from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as clstr

def generate_data(n_samples, flagc):
 if flagc == 1:
    random_state = 365
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
 elif flagc == 2:
    random_state = 148
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) #dodano datasets, X izmjenjen u x radi jednostavnosti
    transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
    x = np.dot(x, transformation)
 elif flagc == 3:
    random_state = 148
    x, y = datasets.make_blobs(n_samples=n_samples, centers = 4, cluster_std=[1.0, 2.5, 0.5, 3.0],random_state=random_state) #dodano centers = 4 (nije radilo bez)
 elif flagc == 4:
    x, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
 elif flagc == 5:
    x, y = datasets.make_moons(n_samples=n_samples, noise=.05)
 else:
    x = []

 return x

clusters = np.arange(2, 21, 1) #broj clustera
gen_datas = np.arange(1, 6, 1) #flagc iz generate_data kako bi se prošlo kroz svih 5 primjera
colors = ['darkmagenta', 'rebeccapurple', 'navy', 'salmon', 'blueviolet']
for gen_data in gen_datas:
    data = generate_data(500, gen_data)
    inertia_s = []
    for cluster in clusters:
        kmeans = clstr.KMeans(cluster).fit(data)
        inertia_s.append(kmeans.inertia_) #sum of squared distances of samples to their closest cluster center
    plt.figure(gen_data)
    plt.plot(clusters, inertia_s, c = colors[gen_data-1])
    plt.xlabel('Broj clustera')
    plt.ylabel('Kriterijska f-ja')
    plt.title('Graf za skupinu podataka ' + str(gen_data))
plt.legend()
plt.show()