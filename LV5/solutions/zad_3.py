# Primijenite hijerarhijsko grupiranje na podatke korištene u Zadatku 1 pomoću funkcije 
# linkage koja je ugrađena scipy metoda za agglomerative clustering:
# from scipy.cluster.hierarchy import dendrogram, linkage
# Prikažite pripadni dendogram. Mijenjajte korištenu metodu (argument method). 
# Kako komentirate postignute rezultate? 
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as clstr
from scipy.cluster.hierarchy import dendrogram, linkage

#kopirano iz zad_1
def generate_data(n_samples, flagc):
 if flagc == 1:
    random_state = 365
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
 elif flagc == 2:
    random_state = 148
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) #dodano datasets, X izmjenjen u x radi jednostavnosti
    transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
    x = np.dot(x, transformation)
 elif flagc == 3:
    random_state = 148
    x, y = datasets.make_blobs(n_samples=n_samples, centers = 4, cluster_std=[1.0, 2.5, 0.5, 3.0],random_state=random_state) #dodano centers = 4 (nije radilo bez)
 elif flagc == 4:
    x, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
 elif flagc == 5:
    x, y = datasets.make_moons(n_samples=n_samples, noise=.05)
 else:
    x = []

 return x

linkage_method = ['single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward'] #svi mogući parametri method

data = generate_data(40, 4) #manji broj podataka radi preglednosti i brzine izvođenja programa, flagc proizvoljno izaberen

for i in range(7):
    kage = linkage(data, linkage_method[i])
    plt.figure(i)
    dendrogram(kage)
    plt.title("Dendrogram za podatke flagc == 1 i metodom " + linkage_method[i])

'za prikaz svih metoda za svaki flagc parametar'
# fig_num=0
# for i in range(5):
#     data = generate_data(40, i+1)
#     for j in range(7):
#        kage = linkage(data, linkage_method[j])
#        plt.figure(fig_num)
#        fig_num = fig_num + 1
#        dendrogram(kage)
#        plt.title("Dendrogram za podatke flagc == " + str(i+1) + " i metodu "+ linkage_method[j])