# Primijenite scikit-learn kmeans metodu za kvantizaciju boje na slici example.png koja 
# se nalazi u
# rusu_lv_2019_20/LV5/resources/. Prikažite originalnu i kvantiziranu sliku. 

from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from collections import Counter

image = mpimg.imread('../resources/example.png') 

X = image.reshape((-1, 1)) 

clusters = np.arange(2, 13, 1)

plt.figure(1)
plt.imshow(image, cmap='gray')
plt.title("Orginalna slika")
unique_val = np.unique(image) #sprema unique values slike u novi array
print(len(unique_val)) #pokazuje koliko tonova slika koristi


# većina koda iz pr 2
for clu in clusters:
   kmeans = cluster.KMeans(n_clusters= clu, n_init=1).fit(X)
   values = kmeans.cluster_centers_.squeeze()
   labels = kmeans.labels_
   image_compressed = np.choose(labels, values)
   image_compressed.shape = image.shape
   unique_val = np.unique(image_compressed)
   print(len(unique_val)) 

   plt.figure(clu)
   plt.imshow(image_compressed, cmap='gray')
   plt.title("Slika sa " + str(clu) + " clustera")
