# Na temelju izraza pseudokoda iz Opisa vježbe implementirajte kmeans algoritam. 
# Primijenite implementirani algoritam na podatke kao u Zadatku 1 te zaključite jeste 
# li pravilno napravili implementaciju algoritma. Modificirajte programski kod tako 
# da u svakoj iteraciji algoritma u polje pohranite i izračunate vrijednosti centara
# klastera. Primijenite programski kod na podatke kao u Zadatku 1 te prikažite u ravnini
# podatke i izračunate centre u svakoj iteraciji algoritma (npr. spojite izračunate 
# vrijednosti centara linijama kako bi vizualizirali „kretanje“ centara kroz iteracije 
# kmeans algoritma).
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as clstr
from scipy.cluster.hierarchy import dendrogram, linkage

def generate_data(n_samples, flagc):
 if flagc == 1:
    random_state = 365
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
 elif flagc == 2:
    random_state = 148
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) #dodano datasets, X izmjenjen u x radi jednostavnosti
    transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
    x = np.dot(x, transformation)
 elif flagc == 3:
    random_state = 148
    x, y = datasets.make_blobs(n_samples=n_samples, centers = 4, cluster_std=[1.0, 2.5, 0.5, 3.0],random_state=random_state) #dodano centers = 4 (nije radilo bez)
 elif flagc == 4:
    x, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
 elif flagc == 5:
    x, y = datasets.make_moons(n_samples=n_samples, noise=.05)
 else:
    x = []

 return x

# distances = np.empty((x.shape[0], y.shape[0]))
#     for i in range(y.shape[0]):
#         for j in range(x.shape[0]):
#             distances[j,i] = np.sqrt(np.sum((y[i,:] - x[j,:])**2))
#     return distances

def dariaKMeans(data, k):
#prvo nasumično odabrati k centroide, gdje je k jednak odabranom broju clustera
#centroidi su data points koji prestavljaju centar clustera (ovdje su centroidi imenovani cluster_centers_ poput atributa klase sklearn.cluster.KMeans)
    cluster_centers_ = data[np.random.choice(data.shape[0], size=k, replace=False)] 
    cluster_centers_list = [cluster_centers_.copy()] 
    labels_ = np.empty(data.shape[0], dtype=int)
    n = 0
#nakon toga se treba dodijeliti svaki podatak svojem najbližem centroidu
    distances = np.empty((data.shape[0], cluster_centers_.shape[0]))
    for i in range(cluster_centers_.shape[0]):
        for j in range(data.shape[0]):
            distances[j,i] = np.sqrt(np.sum((cluster_centers_[i,:] - data[j,:])**2)) #formula za udaljenost dvije točke kordinatne ravnine
#zatim izračunati nove centroide koji predstavaljaju mean clustera
#prvo račujanje novih centroida obaviti izvan while petlje kako bi se mogao postaviti uvjet za while
    for i in range(labels_.shape[0]):
            labels_[i] = np.argmin(distances[i,:])
    for i in range(k):
            cluster_centers_[i] = np.mean(data[labels_==i], axis=0)
#obavljati 2. i 3. korak dok god se dobivaju drukčije vrijednosti za centroide 
    while (cluster_centers_ != cluster_centers_list[n]).all():   
        cluster_centers_list.append(cluster_centers_.copy())
        n = n + 1
        for i in range(cluster_centers_ .shape[0]):
            for j in range(data.shape[0]):
                distances[j,i] = np.sqrt(np.sum((cluster_centers_ [i,:] - data[j,:])**2))
        for i in range(labels_.shape[0]):
            labels_[i] = np.argmin(distances[i,:])
        for i in range(k):
            cluster_centers_[i] = np.mean(data[labels_==i], axis=0)
        if (cluster_centers_ == cluster_centers_list[n]).all():
            break;
    return cluster_centers_list, labels_ #mora vraćati dvije varijable jer su potrebni svi centri i grupiranje podataka po clusterima kako bi se kasnije napravio plot
#za običan kmeans samo treba vratiti cluster_centers_ umjesto .._list

num = [3, 3, 4, 2, 2] 

for n in range(5):
    np.random.seed(10)
    data=generate_data(500, n+1)
    cluster_centers_list, labels_ = dariaKMeans(data, num[n])
    plt.figure(n)
    plt.scatter(data[:, 0], data[:, 1], c = labels_)
    for cluster_center_ in cluster_centers_list:
        plt.scatter(cluster_center_[:,0], cluster_center_[:,1], c = 'red', marker = '*')
    center_list_size = len(cluster_centers_list) -1 # -1 pošto u plot uzima element ispred (i+1)
    for i in range(center_list_size):
        for j in range(num[n]):           
            plt.plot([cluster_centers_list[i][j][0], cluster_centers_list[i+1][j][0]], [cluster_centers_list[i][j][1], cluster_centers_list[i+1][j][1]], c ='salmon')


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    