# Izvještaj LV6

## Opis vježbe
 Kroz vježbu se upoznajemo sa meo+todama klasifikacije: logistička regresija i metoda najbližih susjeda što je još uvijek
 nadgledano učenje.

## Zadatak 1.
 U ovom zadatku bilo je potrebno generirati dva skupa, za učenje i treniranje, sa po 200 i 100 uzoraka. U oba slučaja
 koristit np.random.seed(242).

## Zadatak 2.
 U ovom zadatku trebali su se generirati isti podatci i prikazati pomoću scatter razliku u vrijednosti 3.stupca (je li 0 ili 1).

## Zadatak 3.
 Bilo je potrebno pomoću koeficijenata plotati granicu odluke.
 Rezultat nije dobar pošto je granica odluke pravac i zbog toga ne dijeli dobro podatke.
  
## Zadatak 4.
 Prikazom vjerodostojnosti pomoću zadanog koda dobije se rezultat sličan prošlom zadatku, a time i netočan.

## Zadatak 5.
 Provođenjem klasifikacije testnog skupa podataka dobije se 10 netočno klasificiranih podataka.

## Zadatak 6.
 Korištenje confusion matrix za prikaz točnosti podijele daje rezultate poput rezultata prethodnog zadatka. Prikazani su i 
 zadani pokazatelji.

## Zadatak 7.
 U ovom zadatku bilo je potrebno primijeniti polinomsku transformaciju i ponoviti zadatke 3, 4, 5, i 6. Nakon primjene dobije se
 granica odluke koja više nije linearna i točno dijeli podatke. 

## Zadatak 8.
 Primjena kNN daje dobre rezultate, ali je vrijeme izvođenja dugo. Korištenjem 3 susjeda dobije se 100% točnost, dok uzimanjem 5 i više
 se smanjuje.
