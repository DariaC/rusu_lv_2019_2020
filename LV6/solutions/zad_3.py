import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data


np.random.seed(242)
train_data=generate_data(int(200))

logReg = lm.LogisticRegression().fit(train_data[:, :2], train_data[:, 2])

coef = logReg.coef_        
intercept = logReg.intercept_ 

print("theta 1 i theta 2: " + str(coef[0]))
print("theta 0: " + str(intercept[0]))

# granica odlike - decision boundary
x_axis = np.array([train_data[:, 0].min(), train_data[:, 0].max()])
print(x_axis)

# intercept & gradient 
c = -intercept[0]/coef[0][1]
m = -coef[0][0]/coef[0][1] 
y_axis = m*x_axis + c

plt.scatter(train_data[:, 0], train_data[:, 1], c = train_data[:, 2], cmap = 'Pastel2')
plt.scatter(x_axis, y_axis, c = 'black')
plt.plot(x_axis, y_axis, c = "black")

plt.xlabel("X 1")
plt.ylabel("X 2")














































