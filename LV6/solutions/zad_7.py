import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as metrics
from sklearn.preprocessing import PolynomialFeatures

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)
train_data = generate_data(200)
np.random.seed(242)
test_data = generate_data(100)

poly = PolynomialFeatures(degree = 3, include_bias = False)
train_new = poly.fit_transform(train_data[:, :2])
train_new = np.append(train_new, train_data[:, 2:3], axis=1)
test_new = poly.fit_transform(test_data[:, :2])
test_new = np.append(test_new, test_data[:, 2:3], axis=1)
print(train_new.shape)
print(test_new.shape)

logReg = lm.LogisticRegression().fit(train_new[:, :9], train_new[:, 9])

x_grid, y_grid = np.mgrid[min(train_data[:, 0]) - 0.5 : max(train_data[:, 0]) + 0.5 : .05,
                          min(train_data[:, 1]) - 0.5 : max(train_data[:, 1]) + 0.5 : .05]

grid = np.c_[x_grid.ravel(), y_grid.ravel()]
grid = poly.fit_transform(grid)
probs = logReg.predict_proba(grid)[:, 1].reshape(x_grid.shape)

# zad 3 - granica odluke
plt.figure(1)
cont = plt.contour(x_grid, y_grid, probs, 60, cmap = 'Pastel1', vmin=0, vmax=1, levels=[.5])

plt.scatter(train_data[:, 0], train_data[:, 1], c = train_data[:, 2], cmap = 'Paired')

plt.xlabel('$x_1$', alpha=0.9)
plt.ylabel('$x_2$', alpha=0.9)
plt.title('Granica odluke')

# zad 4 - izlaz logičke regresije 
f, ax = plt.subplots(figsize=(8, 6))

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Blues", vmin=0, vmax=1)

ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')

plt.scatter(train_data[:, 0], train_data[:, 1], c = train_data[:, 2], cmap= 'Set2')
plt.show()


# zad 5 - klasifikacija
plt.figure(3)

predictions = logReg.predict(test_new[:, :9])
real = test_data[:, 2]
test_colors = ['mediumseagreen' if real[i] == predictions[i] 
               else 'black' for i in range(len(real))]

cont = plt.contour(x_grid, y_grid, probs, 60, cmap = 'gist_earth', vmin=0, vmax=1, levels=[.5])
plt.scatter(test_data[:,0], test_data[:, 1], c = test_colors)
plt.xlabel('$x_1$', alpha=0.9)
plt.ylabel('$x_2$', alpha=0.9)


# zad 6 -  confusion matrix + pokazatelji
plot_confusion_matrix(metrics.confusion_matrix(test_data[:, 2], predictions))

tn, fp, fn, tp = metrics.confusion_matrix(test_data[:, 2], predictions).ravel()

accuracy = (tp + tn) / (tp + tn + fp + fn)
missclassification_rate = 1 - accuracy
precision = tp / (tp + fp)
recall = tp / (tp + fn)
specificity = tn / (tn+fp)

print('accuracy = ' + str(accuracy))
print('missclassification rate = ' +  str(missclassification_rate))
print('precision = ' + str(precision))
print('recall/sensitivity = ' + str(recall))
print('specificity = ' + str(specificity))
