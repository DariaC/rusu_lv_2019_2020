# Izvještaj LV7

## Opis vježbe
 Kroz vježbu se upoznajemo sa neuromskim mrežama i njihovom uporabom.

## Zadatak 1.
 U ovom zadatku bilo je potrebno generirati skupove podataka i neuronsku mrežu. Podatke prije generiranje mreže treba standardizirati.
 Kroz dvije for petlje mijenjaju se parametri broj neurona i alpha za dvoslojnu neuronsku mrežu. Neuronska mreža daje reultate slične 
 logističkoj regresiji.

## Zadatak 2.
 U ovom zadatku bilo je potrebno usporediti rezultate neuronske mreže i logističke regresije. Ovdje sam mijenjala samo
 broj neruona u dvoslojnoj neuronskoj mreži. Rezultati su prikazani pomoću plot funkcije za orginalne podatke, logističku regresiju i 
 neuronsku mrežu. Dobiju se poprilično slični rezultati za neke parametre neuronske mreže, a u nekim slučajevima i bolji.
 

