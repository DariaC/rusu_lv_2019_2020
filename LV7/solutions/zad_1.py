import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.neural_network import MLPClassifier
import sklearn.metrics as metrics
import sklearn.linear_model as lm

def generate_data(n):
	
	#prva klasa
	n1 = int(n/2)
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
	
	#druga klasa
	n2 = int(n - n/2)
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)
	
	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]
	
	return data

def plot_confusion_matrix(c_matrix, size):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom sa ' + str(size) + ' neurona')
    plt.show()

#funkcija za crtanje desicion boundary 
def plot_decision_boundary(train_data, nn, size, alpha):
    x_grid, y_grid = np.mgrid[min(train_data[:, 0]) - 0.5 : max(train_data[:, 0]) + 0.5 : .05,
                          min(train_data[:, 1]) - 0.5 : max(train_data[:, 1]) + 0.5 : .05]

    grid = np.c_[x_grid.ravel(), y_grid.ravel()]
    probs = nn.predict_proba(grid)[:, 1].reshape(x_grid.shape)
    plt.contour(x_grid, y_grid, probs, 60, cmap = 'Greys', vmin=0, vmax=1, levels=[.5])

    plt.scatter(train_data[:, 0], train_data[:, 1], c = train_data[:, 2], cmap = 'Paired')

    plt.xlabel('$x_1$', alpha=0.9)
    plt.ylabel('$x_2$', alpha=0.9)
    plt.title('Granica odluke za '+ str(size) + ' neurona i alpha ' + str(alpha))
    plt.show()

#funkcija za ispis pokazatelja modela
def print_cm_ravel(test_data, size):
    tn, fp, fn, tp = metrics.confusion_matrix(test_data[:, 2], predictions).ravel()
    
    accuracy = (tp + tn) / (tp + tn + fp + fn)
    missclassification_rate = 1 - accuracy
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    specificity = tn / (tn+fp)
    print('Pokazatelji za ' + str(size) + ' neurona:')
    print('accuracy = ' + str(accuracy))
    print('missclassification rate = ' +  str(missclassification_rate))
    print('precision = ' + str(precision))
    print('recall/sensitivity = ' + str(recall)) 
    print('specificity = ' + str(specificity))
    
    
np.random.seed(12)
#generitanje train podataka
train_data = generate_data(200)
#standardizacija podataka (algoritmi učenja općenito profitiraju od standardizacije podatkovnog skupa)
train_data = preprocessing.scale(train_data)
np.random.seed(12)
test_data = generate_data(100)
test_data = preprocessing.scale(test_data)

#odabir broja neurona za neuronske mreže i parametre alpha
neuron_num = [2, 10, 20, 40]
alpha = [0.0001, 1]

for i in range(len(neuron_num)):
    for j in range(len(alpha)):
        #stvaranje neuronske mreže od dva sloja
        neural_network = MLPClassifier(hidden_layer_sizes=(neuron_num[i], neuron_num[i]), alpha = alpha[j]).fit(train_data[:, 0:2], train_data[:, 2])
        #predviđanje 
        predictions = neural_network.predict(test_data[:, 0:2])
        #crtanje granice odluke
        plot_decision_boundary(train_data, neural_network, neuron_num[i], alpha[j])
    #crtanje confusion matrix
    plot_confusion_matrix(metrics.confusion_matrix(test_data[:, 2], predictions), neuron_num[i])
    #ispis pokazatelja modela
    print_cm_ravel(test_data, neuron_num[i])
    




































