import numpy as np
from sklearn.preprocessing import PolynomialFeatures
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T

#generiranje podataka
np.random.seed(12)
train_data = non_func(200)
np.random.seed(12)
test_data = non_func(100)

#generiranje lin. model sa pol. članom
poly = PolynomialFeatures(1, include_bias = False) #namješten degree
train_data_poly = poly.fit_transform(train_data)
test_data_poly = poly.fit_transform(test_data)
linReg = lm.LinearRegression().fit(train_data_poly[:, :-1], train_data_poly[:, -1])
predictions_lin = linReg.predict(test_data_poly[:, :-1])

#određivanje broja neurona u mreži
neuron_num = [5, 8, 10, 20, 60]



for i in range(len(neuron_num)):
   #generiranje višeslojne (2 sloja) neuronske mreže sa promjenjivim brojem neurona
   neural_net = MLPRegressor(hidden_layer_sizes = (neuron_num[i], neuron_num[i]), max_iter = 500).fit(train_data[:,0:2], train_data[:, 2])
   #predviđanje
   predictions = neural_net.predict(test_data[:, 0:2])
   #crtanje rezultata
   plt.title('Broj neurona: ' + str(neuron_num[i]))
   plt.scatter(test_data[:,0], test_data[:, 2], color = 'gainsboro')
   #orginalni podatci
   plt.plot(test_data[:,0], test_data[:, 1], color = 'olivedrab', label = 'Data plot')
   #linearna regresija
   plt.plot(test_data[:,0], predictions_lin, color = 'purple', label = 'Linear regression')
   #neuronska mreža
   plt.plot(test_data[:,0], predictions, color = 'deepskyblue', label = 'Neural network') 
   plt.legend()
   plt.show()
